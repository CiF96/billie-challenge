import { configureStore } from "@reduxjs/toolkit";
import { CompanyReducer } from "./CompanySlice";

export const RootStore = configureStore({
  reducer: {
    company: CompanyReducer
  }
})

export type RootState = ReturnType<typeof RootStore.getState>

export type AppDispatch = typeof RootStore.dispatch