import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface CompanyProfile {
  id: number,
  name: string,
  budget: number,
  budget_spent: number,
  date_of_first_purchase: string
}

export interface CompanyState {
  companies: CompanyProfile[]
}

export const initialState: CompanyState = {
  companies: [{
    id: 1,
    name: "Martian Firma",
    budget: 10000.0000,
    budget_spent: 4500.0000,
    date_of_first_purchase: "2119-07-07",
  }, {
    id: 2,
    name: "Solar Firma",
    budget: 1123.2200,
    budget_spent: 451.3754,
    date_of_first_purchase: "2120-01-14",
  }, {
    id: 3,
    name: "Yellow Corp.",
    budget: 1000000.0000,
    budget_spent: 1000.0000,
    date_of_first_purchase: "2121-12-24",
  },
  ],
};
const companySlice = createSlice({
  name: 'company',
  initialState,
  reducers: {
    updateTotalBudget: (state, action: PayloadAction<{ id: number, newBudget: number }>) => {
      const selectedCompany = state.companies.find(company => company.id === action.payload.id);
      if (selectedCompany == null) {
        return;
      }
      selectedCompany.budget = action.payload.newBudget;
    }
  },
});
export const { updateTotalBudget } = companySlice.actions;
export const CompanyReducer = companySlice.reducer;