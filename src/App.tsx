import React, { useState } from 'react';
import './App.css';
import { BudgetModal } from './features/BudgetModal/BudgetModal';
import { useAppSelector } from './hooks/duckHooks';
import { CompanyProfileItem } from './features/CompanyProfileList/CompanyProfileItem';

export interface CompanyProfile {
  id: number,
  name: string,
  budget: number,
  budget_spent: number,
  date_of_first_purchase: string
}

export const App = function App() {
  const [shouldShowBudgetModal, setShouldShowBudgetModal] = useState(false);
  const [selectedCompany, setSelectedCompany] = useState<CompanyProfile | undefined>(undefined);
  const companies = useAppSelector(state => state.company.companies);

  return (
    <div className="flex min-h-screen w-screen bg-gray-700 justify-center">
      <ul className="p-2.5 w-full sm:w-5/6 md:w-9/12 lg:w-1/2">
        {companies.map((companyProfile: CompanyProfile, index) => {
          const { id } = companyProfile;
          const numberOfCompanyProfiles = companies.length;

          return (
            <React.Fragment key={id}>
              <CompanyProfileItem companyProfile={companyProfile} onClick={() => {
                setShouldShowBudgetModal(true);
                setSelectedCompany(companyProfile)
              }} />
              {
                index + 1 < numberOfCompanyProfiles && <div className="h-2" />
              }
            </React.Fragment>
          )
        })}
      </ul>
      {
        shouldShowBudgetModal && selectedCompany != null &&
        <BudgetModal selectedCompany={selectedCompany} onCloseClick={() => {
          setSelectedCompany(undefined);
          setShouldShowBudgetModal(false);
        }} onSubmitClick={() => {
          setShouldShowBudgetModal(false);
          setSelectedCompany(undefined);
        }} />
      }
    </div>
  );
}

