import React from "react";
import { CompanyProfile } from "src/App";

export interface SuccessMessageProps {
  onContinueClick: () => any
  company: CompanyProfile
}

export const SuccessMessage = function SuccessMessage({ onContinueClick, company }: SuccessMessageProps) {
  return (
    <div className="absolute h-screen w-screen bg-black bg-opacity-50 justify-center items-center flex">
      <div className="rounded bg-white w-full sm:w-5/6 md:w-9/12 lg:w-5/12">
        <div className="items-center justify-between flex flex-row px-8 py-4">
          <p className="text-red-500 font-bold text-xl">
            Success
          </p>
        </div>
        <hr />
        <div className=" px-8 py-4 ">
          <p>You have successfully updated the total budget of the company: {company.name}!</p>
          <div className="flex justify-end">
            <button onClick={onContinueClick} autoFocus type="submit" className="mt-4 rounded shadow-md px-4 py-3 bg-red-500 text-white hover:bg-red-300">Continue</button>
          </div>
        </div>
      </div>
    </div>
  )
}