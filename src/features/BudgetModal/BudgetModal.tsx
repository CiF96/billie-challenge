import React, { useState } from "react";
import { XIcon, ExclamationCircleIcon } from '@heroicons/react/outline'
import _ from "lodash";
import * as yup from "yup";
import { Formik } from "formik";

import { CompanyProfile } from "src/App"
import { updateTotalBudget } from "src/ducks/CompanySlice";
import { useAppDispatch } from "src/hooks/duckHooks";
import { SuccessMessage } from "../SuccessMessage/SuccessMessage";

export interface BudgetModalProps {
  selectedCompany: CompanyProfile;
  onCloseClick: () => any;
  onSubmitClick: () => any;
}

export const BudgetModal = function BudgetModal({ selectedCompany, onCloseClick, onSubmitClick }: BudgetModalProps) {
  const [shouldShowSuccessMessage, setShouldShowSuccessMessage] = useState(false);
  const companyBudgetSpentIntl = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(selectedCompany.budget_spent)
  const dispatch = useAppDispatch();

  const validationSchema = yup.object({
    newBudget: yup
      .number()
      .moreThan(selectedCompany.budget_spent, `New budget must be higher than ${companyBudgetSpentIntl}`)
      .required("Amount is required")
      .positive()
      .typeError("Offered price must be a number")
  })

  return (
    <div className="absolute h-screen w-screen bg-black bg-opacity-50 justify-center items-center flex px-6 py-6">
      <div className="rounded bg-white w-full sm:w-5/6 md:w-9/12 lg:w-5/12">
        <div className="items-center justify-between flex flex-row px-8 py-4">
          <p className="text-red-500 font-bold text-xl">
            {selectedCompany.name}
          </p>
          <XIcon className="h-6 w-6 cursor-pointer" onClick={onCloseClick} />
        </div>
        <hr />
        <div className="px-8 py-4">
          <p className="text-l mb-2">
            Please enter the desired total budget of this company in the input below
          </p>
          <Formik
            initialValues={{ newBudget: selectedCompany.budget.toFixed(2) }}
            onSubmit={(values, actions) => {
              dispatch(updateTotalBudget({ id: selectedCompany.id, newBudget: _.toNumber(values.newBudget) }))
              setShouldShowSuccessMessage(true);
            }}
            validationSchema={validationSchema}
          >
            {props => (
              <form onSubmit={props.handleSubmit}>
                <div className="mt-4">
                  <label htmlFor="newBudget" className="block text-sm font-medium text-gray-700">
                    New Total Budget
                  </label>
                  <div className="mt-1 relative rounded-md shadow-sm">
                    <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                      <span className="text-gray-500">€</span>
                    </div>
                    <input
                      autoFocus
                      type="text"
                      name="newBudget"
                      id="newBudget"
                      className={`${props.errors.newBudget && "border-red-500"} ${props.errors.newBudget ? "focus:border-red-700" : "focus:border-red-500"} focus:ring-red-500 block w-full pl-7 pr-12 border px-3 py-2 rounded-md`}
                      placeholder="0.00"
                      value={props.values.newBudget}
                      onChange={props.handleChange}
                    />
                    {props.errors.newBudget &&
                      <div className="absolute inset-y-0 right-0 flex items-center pointer-events-none">
                        <ExclamationCircleIcon className="text-red-500 h-6 w-6 mr-2 " />
                      </div>
                    }
                  </div>
                  {props.errors.newBudget && <p className="text-sm text-red-700">{props.errors.newBudget}</p>}
                </div>
                <div className="flex justify-end">
                  <button type="submit" disabled={props.errors.newBudget ? true : false} className={`mt-4 rounded shadow-md px-4 py-3 ${props.errors.newBudget ? 'bg-red-300' : 'bg-red-500'} text-white hover:bg-red-300`}>Submit</button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
      {shouldShowSuccessMessage &&
        <SuccessMessage company={selectedCompany} onContinueClick={() => {
          onSubmitClick();
          setShouldShowSuccessMessage(false);
        }} />
      }
    </div>
  );

}