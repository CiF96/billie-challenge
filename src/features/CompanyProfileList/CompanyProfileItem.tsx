import React from "react";
import { ChevronRightIcon } from "@heroicons/react/outline";
import dayjs from "dayjs";
import { CompanyProfile } from "src/ducks/CompanySlice";

export interface CompanyProfileItemProps {
  companyProfile: CompanyProfile;
  onClick: () => any
}

export const CompanyProfileItem = function CompanyProfileItem({ companyProfile, onClick }: CompanyProfileItemProps) {
  const { name, budget, budget_spent: budgetSpent, date_of_first_purchase: dateOfFirstPurchase } = companyProfile;

  const companyBudgetIntl = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(budget);
  const companyBudgetSpentIntl = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(budgetSpent)
  const dateOfFirstPurchaseFormated = dayjs(dateOfFirstPurchase).format('MMMM D, YYYY')

  return (

    <li>
      <div onClick={onClick} className="flex flex-row rounded px-8 py-8 bg-white hover:bg-gray-400 cursor-pointer w-full items-center justify-between">
        <div>
          <p className="text-red-500 font-bold text-xl">
            {name}
          </p>
          <p className="text-l">Budget: {companyBudgetIntl}</p>
          <p className="text-l">Budget spent: {companyBudgetSpentIntl}</p>
          <p className="text-l">Date of first purchase: {dateOfFirstPurchaseFormated}</p>
        </div>
        <ChevronRightIcon className="h-6 w-6" />
      </div>
    </li>

  )
}